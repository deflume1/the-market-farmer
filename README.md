# The Market Farmer #

Welcome to the The Market Farmer repo!  Let's use the Bitbucket readme template, since it seems nice.

### What is this repository for? ###

* Holds the client side and server side code for The Market Farmer
* Version 1.0

### How do I get set up? ###

The code is written to deploy to a CentOS 7 instance hosted on AWS or to Heroku.  I've included a
configuration script in /aws, but you should be able to deploy this anywhere, as long as you have the
dependencies installed in that script satisfied.

Server side code, including raw dust templates, lives in /src.  Client side code lives in /static.

Here's how it generally works:

AWS:

1. Setup AWS instance with init_instance.bash (or install packages manually)
2. Checkout project
3. Run 'npm install'
4. Start server with a command like 'sudo scripts/start_server.sh 80'

Heroku:

1. Fork project
2. Connect heroku to your git repository
3. Setup automatic deploy
4. Deploy
   * See https://devcenter.heroku.com/articles/git for more details

A node.js process will start.  It will:

1. Read and compile all the templates under /src/dust/templates
2. Save the compiled templates to /static/js/
3. Start the webserver

At this point you're good to go.

### How does it work? ###

I used Node.js to write a webserver that proxies and caches calls to a USDA API for farmer's market data.  The API data returned isn't always usable, so the server does some transformations on it.  Then, it sends it to the client.

The client uses Dust.js and the returned data to render the relevant portions of the page.  The client only ever talks to the node server, it doesn't talk to the USDA API directly.  Theoretically, this should let us swap in any data source for farmer's markets without having to make client changes, and hopefully avoid overwhelming the USDA API.

jQuery and the Google Maps v3 JavaScript API are used on the client for ease of use, cross-browser compatibility, and obviously map rendering.

I'd be wary of adding any kind of 'bulk' call to the API.  One of the major limitations here (and something I'd like to address down the road) is the inability to display more than one pin at a time.  THe API only allows you to fetch one set of market details at a time, and the details are what have the lat/lng on them.  If you do decide to build out something like this, I don't know if you'll break the API (it is the USDA, after all).

### Directories ###

*  src - Node.js and dust.js code/templates
*  static - CSS and JS, including compiled dust templates
*  aws - Instance init script for CentOS7 AWS instance
*  reference - A brief primer on the USDA API
*  scripts - Convenience script for starting the server on a given port

### Contribution guidelines ###

This isn't really an 'open' project so much as it is a playground, but if you *really* want to contribute, feel free to make a pull request, or message me.

### Who do I talk to? ###

* Author: Christopher A DeFlumeri
* Email: Chris@deflumeri.com (or deflume1@gmail.com)

### Credit to: ###
* CentOS 7
* Amazon Web Services
* Heroku
* Node.js
* dust.js (LinkedIn)
* jQuery
* Google Maps API v3
