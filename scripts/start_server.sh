#!/bin/bash
#
# Start The Market Farmer main server
# 
# @author cdeflumeri
# @since 2/2015

# Check for TMFTOP
if [ -z "$TMFTOP" ]; then
  SCRIPTPATH=$(readlink -f $0)
  TMFTOP="$(dirname $(dirname $SCRIPTPATH))"
fi

# Default to 8080
PORT=8080

# Locate the node executable (different for Red Hat and Ubuntu)
NODE=/usr/bin/node
if [ ! -f $NODE ]; then
  NODE=/usr/bin/nodejs
fi

# Check for usage
if [ "$1" == "help" -o "$1" == "usage" ]; then
  echo ""
  echo "Starts the main The Market Farmer web server"
  echo ""
  echo "Usage:"
  echo "    start_server.sh <optional port number>"
  echo ""
  exit 1
fi

# Check to see if we need to be root
if [ -n "$1" ]; then
  PORT="$1"
  if [ "$1" -le "1024" ]; then
    if [ $EUID -ne 0 ]; then
      echo ""
      echo "To start the server on a privleged port, you must run this script with root privleges"
      echo ""
      exit 1
    fi
  fi
fi

# Start the server
$NODE $TMFTOP/src/init/start.js $PORT
