/**
 *
 * tmf.js
 *
 * The core JS that powers The Market Farmer
 *
 * @author deflume1
 * @since 2/2015
 */
 
 $(function() {

  /**
   * This is the code that powers the interaction between the client
   * and server.  Its responsible for fetching new market data, and 
   * then calling the appropriate dust engine render functions
   */

  var _zipAction = 'zip'
  ,   _locationAction = 'location'
  ,   _detailsAction = 'details'

    /**
     * Try to get a list of markets near a zip code.
     * If successful, render the market list
     */
  , _getMarketsByZipCode = function(/* integer */ code) {
    $.ajax({
       url : _zipAction
     , type : 'GET'
     , data : { zipCode : code }
     , dataType : 'json'
    }).success(function (json) {
      if(json.results && !(json.results.id === 'Error')) {
        _renderMarketList(json, true);
      }
    }).error(function() { });
  }
  
    /**
     * Try to get a list of markets based on a position.
     * If successful, render the market list
     */
  , _getMarketsByPosition = function(/* float */ lat, /* float */lng) {
       $.ajax({
       url : _locationAction
     , type : 'GET'
     , data : { latitude : lat, longitude : lng }
     , dataType : 'json'
    }).success(function (json) {
      _renderMarketList(json, false);
    }).error(function() { }); 
  }

     /**
      * Rerender the list of markets.  Only recenter the map if
      * 'recenter' is true
      */
   , _renderMarketList = function(/* json */ resultsJson, /* boolean */ recenter) {
      dust.render('market_list', resultsJson , function(err, output) {
        $('#MARKET_DATA').html(output);

        // Get the details for the first result if we have one
        if(resultsJson.results && resultsJson.results[0]) {
          var firstResult = resultsJson.results[0];
          _getDetailsById(firstResult.id, firstResult.name, recenter);
        } 
        
        _registerMarketClickEvents();
      });
  }
   
    /**
     * When you click on a market in the list, select it, and
     * initiate the call to get the details for the market
     */
  , _registerMarketClickEvents = function() {
    $('.resultsList .entry').on('click', function(e) {
      e.preventDefault();

      var target = $(e.target).closest('.entry')
        , marketId = target.data('id')
        , marketName = target.data('name')
        ;
        $('.entry').removeClass('selected');
        target.addClass('selected');

        _getDetailsById(marketId, marketName, true);
    });
  }
    
    /**
     * Look up the details for a particular market by id.
     */
  , _getDetailsById = function(/* integer */ marketId, /* string */ marketName, /* boolean */recenter) {
       $.ajax({
       url : _detailsAction
     , type : 'GET'
     , data : { id : marketId }
     , dataType : 'json'
    }).success(function (json) {
      _renderDetails(json, marketId, marketName, recenter);
    }).error(function() { }); 
  }

    /**
     * Render the details for  a particular market.
     * Also decorates the returned results with the market name, since the
     * USDA api doesn't do that for us.  The uses the decorated results to 
     * add a map marker
     */
  , _renderDetails = function(/* json */ resultsJson, /* integer */ marketId, /* string */ marketName, /* boolean */recenter) {
       resultsJson.details.name = marketName;
       dust.render('market_details', resultsJson , function(err, output) {
         $('#MARKET_DETAILS').html(output);
       });
       if(_map) {
          _addMapMarker(_map, resultsJson.details, recenter);
       }
     }

  /**
   * Below is the Google Maps specific code.  Probably worth splitting this
   * out into its own file when I get the time
   */

  , _map = null
  , _marker = null

    /**
     * Initialize a Google Map centered at the given latitude and longitude
     * After its initialized, fetch the markets around its center
     */
  , _initializeGoogleMap = function(/* float */ latitude, /* float */ longitude) {
      var mapDiv = $('#MAP')[0]
        , mapOptions = {
            center:  new google.maps.LatLng(latitude, longitude)
          , zoom: 14
          , mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        , map = new google.maps.Map(mapDiv, mapOptions);
        _setupMapEvents(map);
        _map = map;
        _getMarketsByPosition(latitude, longitude);
     }

    /**
     * Attempt to initialize a map to the user's location.  If the user
     * shares their location, give them to the map initialization callback,
     * otherwise use the coordinates for Brookline, MA
     */
  , _initializeMapWithPosition = function(/* function */ callback) {
      navigator.geolocation.getCurrentPosition(
        function(position) {
          callback(position.coords.latitude, position.coords.longitude);
        }
      , function(error) {
          callback(42.330438, -71.123120); // Brookline, MA
       }
       , {'enableHighAccuracy' : true, 'timeout' : 100, 'maximumAge' : Infinity}
      );
    }

    /**
     * Set the map center to the given latitude and longitude, then
     * fetch markets near that location
     */
  , _setMapCenter = function(/* float */ latitude, /* float */longitude) {
      if(_map) {
        var pos = new google.maps.LatLng(latitude, longitude);
        _map.setCenter(pos);
        _getMarketsByPosition(latitude, longitude);
      }
    }

    /**
     * Setup the 'dragend' event listener so that we fetch new markets
     * whenever the user drags the map
     */
  , _setupMapEvents = function/* Google Map */(map) {
      google.maps.event.addListener(map, 'dragend', function() {
        var center = map.getCenter();
        _getMarketsByPosition(center.lat(), center.lng());
      });
    }
 
    /**
     * Clear all current map markers, and add a new map marker for the given
     * market details. Optionally recenter the map on the marker if 'recenter' is
     * true
     */
  , _addMapMarker = function(/* Google Map */ map, /* json */ details, /* boolean */ recenter) {
      var markerPos = new google.maps.LatLng(details.coordinates.latitude, details.coordinates.longitude)
        , marker = new google.maps.Marker({
        position: markerPos,
        map: map,
        title: details.name
      }); 
      if(_marker) {
        _marker.setMap(null);
      }
      _marker = marker;
      _addInfoWindow(marker, details);
      if(recenter) {
        _map.setCenter(markerPos);
      }
    }

    /**
     * Add a new info window to a marker with a market's name
     * and address.  Open the info window if the user clicks the 
     * marker
     */
  , _addInfoWindow = function/* Google Map Marker */ (mapMarker, /* json */ details) {
      dust.render('info_window', details, function(err, output) {
        var infowindow = new google.maps.InfoWindow({
          content: output
        });

        google.maps.event.addListener(mapMarker, 'click', function() {
          infowindow.open(_map, mapMarker);
        });
      });
    }
  
  ;

  /**
   * Execute the below when the browser fires the 'ready' event
   */

   /**
    * Override the 'submit' event for the zip code form
    * so it does a zip code lookup instead of a GET to some
    * weird URL
    */
   $('#ZIP').on('submit', function(e) {
       e.preventDefault();
       var zipCode = $('#ZIP .zip').val();
       if(!isNaN(zipCode) && (zipCode + "").length == 5) {
         _getMarketsByZipCode(zipCode);
       }
   });

    /**
     * Initialized the Google Map, and center it on Brookline. 
     */
   _initializeGoogleMap(42.330438, -71.123120); // Brookline, MA
   
   /**
    * Try to get the user's position, and use it to setup the map.
    * We do this after the above, since in some browsers (i.e. Firefox)
    * the "Share Location" dialog *never* times out, and apparently this
    * is by design, despite the "timeout" argument passed.  Better to have a 
    * usable, Brookline-centered map initialized than a blank page
    */
   _initializeMapWithPosition(_setMapCenter);
 });
