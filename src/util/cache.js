/**
 * cache.js
 * 
 * A generic, in-memory LRU cache with configurable capacity
 *
 * To use this module:
 *   var cacheBuilder = require('/path/to/cache.js');
 *   var maxCapacity = 500;
 *   var cache = cacheBuilder(maxCapacity);
 *
 * @author deflume1
 * @since 2/2015
 */

module.exports = function (/* integer */ maxCapacity) {

  var DEFAULT_MAX_CAPACITY = 1000

    , _maxCapacity = maxCapacity ? maxCapacity : DEFAULT_MAX_CAPACITY
      
    , _cache = {}

    , _cacheKeys = []

      /**
       * Put a value in the cache with the specified key
       */
    , _put = function(/* string */ key, /* any */ value) {
        if(!key || !value) {
          return;
        }

        var stringKey = key + ""
          , _prevValue = _cache[stringKey]

        if(_cacheKeys.length > _maxCapacity) {
          delete _cache[_cacheKeys.pop()];
        }

        _cache[stringKey] = value;

        _cacheKeys.unshift(stringKey);

        return _prevValue;
      }

        /* Try to get a value out of the cache with the specified
         * key
         */
      , _get = function(/* string */ key) {
          if(!key) {
            return;
          }

          var stringKey = key + "";

          return _cache[stringKey];
       }

       ;

  return {
    /**
     * Put a value into the cache
     *
     * @param key - A cache key
     * @param value - A cache value
     */
    put: function(/* string */ key, /* any */ value) {
      return _put(key, value);
    }
  
   /**
    * Get a value out of the cache
    *
    * @param key- A cache key
    * @return The value corresponding to key if it exists
    */
  , get: function(/* string */ key) {
      return _get(key);
    }
  }
};
