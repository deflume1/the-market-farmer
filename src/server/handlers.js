/**
 * handlers.js
 *
 * This file defines the handler functions for the server
 * request actions.
 *
 * After defining a new handler function, you should register it in
 * the 'handlers' map at the bottom of this file so it can be used
 *
 * Makes use of the node-static library found at https://github.com/cloudhead/node-static
 *
 * @author deflume1
 * @since 2/2015
 */

module.exports = (function() {
  
  var _fetcher = require('../api/fetcher.js')

  , _urlParser = require('url')
  
  , _static = require('node-static')

  , _staticServer = new _static.Server('static/')

  , _dust = require('../dust/dust-base.js')

    /**
     * Serve up the index page, when the user hits themarketfarmer.com
     */
  , _defaultHandler = function(request, response) {
      _dust.renderTemplate('index', {}, function(err, output) {
        if(err) {
          console.log('Error caught in defaultHandler');
          console.log(err);
          return _invalidHandler(request, response);
        }
        _writeResponse(request, response, output);
      });
    }

    /**
     * Serve any resources from the /static directory
     */
  , _staticHandler = function(request, response) {
      try {
        _staticServer.serve(request, response, function(e, res) {
            if(e && e.status == 404) {
              _notFoundHandler(request, response);    
            }
          });
      } catch(ex) {
        _invalidHandler(request, response);
      }
    }

    /**
     * Serve a response for a zip code lookup query
     */
  , _zipHandler = function(request, response) {
      var parts = _urlParser.parse(request.url, true)
        , zipCode = parts.query['zipCode']
        ;

        if(!zipCode) {
          console.log("No zip code found on request object passed to handlers._zipHandler");
          console.log(parts.query);
          return _invalidHandler(request, response);
        }

        _fetcher.fetchIdsByZipCode(
          zipCode.trim() 
        , function(json) {
            _writeJsonResponse(request, response, json);
          }
        , function(error) {
            _invalidHandler(request, response);
          }
        );
     }

    /** 
     * Serve a response for a location (lat/long) lookup query
     */
  , _locationHandler = function(request, response) {
      var parts = _urlParser.parse(request.url, true)
      ,   latitude = parts.query['latitude']
      ,   longitude = parts.query['longitude']
      ;

      if(!latitude || !longitude) {
        console.log("Latitude or longitude not found on request object passed to handlers._locationHandler");
        console.log(parts.query);
        return _invalidHandler(request, response);
      }

      _fetcher.fetchIdsByLocation(
        latitude.trim()
      , longitude.trim()
      , function(json) {
          _writeJsonResponse(request, response, json);
        }
      , function(error) {
          _invalidHandler(request, response);
      });
    }
    
    /**
     * Serve a response for a market details query
     */
  , _detailsHandler = function(request, response) {
      var parts = _urlParser.parse(request.url, true)
      ,   id = parts.query['id']
      ;

      if(!id) {
        console.log("ID not found on request object passed to handlers._detailsHandler");
        console.log(parts.query);
        return _invalidHandler(request, response);
      }

      _fetcher.fetchDetailsForMarketId(
        id.trim()
      , function(json) {
          _writeJsonResponse(request, response, json);
        }
      , function(error) {
          _invalidHandler(request, response);
        }
      );
    }

  , _hudlUHandler = function(request, response) {
      var randomNum = Math.floor(Math.random() * 100000);
      _writeResponse(request, response, '' + randomNum);
    }

    /**
     * Serve a generic 400 error.  Useful if we encounter
     * a request that we can't service since its malformed, etc
     */
  , _invalidHandler = function(request, response) {
      response.writeHead(400, 'Bad Request');
      response.end('400 Bad Request');
    }

    /**
     * Server a generic 404 error.  Useful if we encounter
     * a request to an unknown location/endpoint
     */
  , _notFoundHandler = function(request, response) {
      response.writeHead(404, 'Not Found');
      response.end('404 Not Found');
    }

    /**
    * Given a json object, just write it out as a string to 
    * the response, with the write content type and encoding
    */
  , _writeJsonResponse = function(request, response, json) {
      try {
        var jsonString = JSON.stringify(json);
        response.writeHead(200, { 
          'Content-Length' : jsonString.length
        , 'Content-Type' : 'application/json'
        });
        response.write(jsonString, 'utf8');
        response.end();
      } catch(ex) {
        console.log('Error caught writing response in handlers._writeJsonResponse');
        console.log(ex);
      }
    }

  /**
   * Given a string, just write it out to the resposne with the right
   * encoding
   */
  , _writeResponse = function(request, response, body) {
      try {
        response.writeHead(200);
        response.write(body, 'utf8');
        response.end();
      } catch (ex) {
        console.log('Error caught writing response in handlers._writeResponse');
        console.log(ex);
        console.log(body);
      }
    }

    /**
     * Registed new request handlers here. 
     *  Ex:
     *    www.themarketfarmer.com/myhandler
     *    handlers = { 'myhandler' : functon(request, response) { myHandlerFunction(request, response); }
     */
  , _handlers = {
        'default' : function(request, response) { _defaultHandler(request, response); }
      , 'js' : function(request, response) { _staticHandler(request, response); }
      , 'css' : function(request, response) { _staticHandler(request, response); }
      , 'zip' : function(request, response) { _zipHandler(request, response); }
      , 'location' : function(request, response) { _locationHandler(request, response); }
      , 'details' : function(request, response) { _detailsHandler(request, response); }
      , 'invalidHandler' : function(request, response) { _invalidHandler(request, response); }
      , 'hudlu' : function(request, response) { _hudlUHandler(request, response); } 
    }
    ;

  return {
    /**
     * Look up a specific handler by name.  Handler must be registerd in the 'handlers' map.
     * The returned handler will be a function that accepts arguments (request, response)
     * If handler is not found, a generic 'Bad Request' handler will be returned
     *
     * @param handlerName - The name of a registered request handler
     * @return The named handler function, or a generic Bad Request handler
     */
    getHandler : function(/* string */ handlerName) {
      var handler = _handlers[handlerName];
      if(!handler) {
        handler = _handlers['invalidHandler'];
      }
      
      return handler;
    } 
  }

})();
