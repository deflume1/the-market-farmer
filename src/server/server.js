/**
 * server.js
 *
 * The main application server for The Market Farmer
 *
 * To use this module: 
 *   var server = require('/path/to/server.js')
 *   server.start(80)
 *
 * Note that the main application server is a singleton, and repeated
 * invocations to require will return a reference to the same server
 *
 * @author deflume1
 * @since 2/2015
 */


module.exports = (function() {

  var _http = require('http')

    , _urlParser = require('url')

    , _server = _http.createServer(function(req, res) { _dispatchHandler(req, res) })
 
    , _handlers = require('./handlers.js')

      /**
       * Dispatch the request to the appropriate request handler
       */
    , _dispatchHandler = function(request, response) {
        if(!request || !response) {
          console.log("request or response not defined, can't handle request!");
        }

          var parts = _urlParser.parse(request.url)
            , action = _getAction(parts.pathname)
            , handler = _handlers.getHandler(action);
          
          handler(request, response);
      }
    
    /**
     * Get the request action, the first word following the
     * '/' in the pathname
     *
     * @param pathname - the request pathname string
     * @return the first word after '/' if it exists
     */
    , _getAction = function(/* string */ pathname) {
        if(!pathname) {
          return;
        }

        var splitPathname = pathname.split('/')
          , firstPath = splitPathname[0] ? splitPathname[0] : splitPathname[1];
        
        if(firstPath) {
          return firstPath.trim();
        }

        return 'default';
    }

      /**
       * Simple response handler that writes "Hello World" and serves a 200 response
       * code.  Useful for ensuring the correct application is running
       * @param request - The node.js HTTP request object
       * @param response - The node.js HTTP respons object
       */
    , _helloWorld = function(request, response) {
        if(!request || !response) {
          console.log("request or response not defined, can't handle request!");
        }
        response.writeHead(200);
        response.end('Hello World!');
      }
    ;

  return {

    /**
     * Start the application server on the specified port
     * @param port - The port number to start the server on
     */
    start: function(/* integer */ port) {
      if(!port) {
        port = 8080;
      }
      
      _server.listen(port);
    }
  };
})();
