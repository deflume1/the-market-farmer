/**
 * start.js
 * 
 * Starts the main application server for The Market Farmer
 *
 * @author deflume1
 * @since 2/2015
 */

(function(args) {
  var server = require('../server/server.js')
    , dust = require('../dust/dust-base.js')
    , port = (args && args.length > 2) ? args[2] : 80;
 
  dust.init(); // Read/compile/copy all the templates
  if(process.env.PORT) {
    console.log('process.env.PORT detected, deploying on heroku, ignoring stdin specified port (if any)');
    port = process.env.PORT;
  }

  server.start(port); // Start the server
  console.log("Server started on " + port);
})(process.argv);
