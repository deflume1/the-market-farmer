/**
 *
 * dust-base.js
 *
 * Controls the compilation and loading of dust templates
 * for The Market Farmer
 *
 * Uses dustjs-linkedin and dustjs-helpers from http://linkedin.github.io/dustjs/
 *
 * @author deflume1
 * @since 2/2015
 */

module.exports = (function() {

    var _dust = require('dustjs-linkedin')
      
      , _fs = require('fs')

      , _templatePath = 'src/dust/templates/'
    
      , _compiledTemplatePath = 'static/js/'
       
        /**
         * Read all the templates, compile them, then write the
         * compiled versions to static/js
         */
      , _init = function() {
          
          var files = _fs.readdirSync(_templatePath)
            , index
            , filename
            , file
            , template
            , templateName;

          for(index = 0; index < files.length; index++) {
            filename = files[index];
            if(filename.indexOf('.dust') !== -1) {
              file = _fs.readFileSync(_templatePath + filename, { encoding : 'utf8' });
              templateName = filename.slice(0, -5).trim();
              template = _dust.compile(file, templateName);
              _fs.writeFileSync(_compiledTemplatePath + templateName + '.js', template, { encoding : 'utf8'});
              _dust.loadSource(template); 
              console.log('Compiled and registered template ' + templateName);
            }
          }
        }
       
       /**
        * Get a template out of the dust cache
        */
     , _getTemplate = function(/* string */ templateName) {
         if(!templateName) {
           return;
         }

         return _dust.cache(templateName);
       }

     ;

  return {
      /**
       * Render a dust template with the given name and context.  Then execute the 
       * callback that takes arguments (error, output)
       *
       * @param templateName - The name of a registered dust template
       * @param dustContext - The context used to render the template
       * @param callback - The callback to execute on the output of the render
       */
      renderTemplate : function(/* string */ templateName, /* object */ dustContext, /* function */ callback) {
        _dust.render(templateName, dustContext, callback); 
      }

      /**
       * Retrieve a compiled dust template by name
       *
       * @param templateName - The name of the compiled template
       * @return The compiled template, if it exists
       */
    , getTemplate : function(/* string */ templateName) {
        return _getTemplate(templateName);
      }

      /**
       * Initialize the dust system.  Reads all of the templates in
       * src/dust/templates, then compiles them, and indexes them by
       * filename minus the '.dust' suffix.
       *
       * Repeated invocations will flush the dust cache
       */
    , init : function() {
        _init();
    }
  };
})();
