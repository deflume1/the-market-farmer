/**
 * fetcher.js
 *
 * Responsible for fetching data from USDA Farmer's Market API
 *
 * Makes use of the requests library from https://github.com/request/request
 *
 * @author deflume1
 * @since 2/2015
 */

module.exports = (function() {

  var _request = require('request')

    , _cacheBuilder = require('../util/cache.js')

    , _cache = _cacheBuilder(5000)

    , _fetchIdsByZipCode = function(/* integer */ zipCode, /* function */ callback, /* function */ errorHandler) {
        if(!zipCode) {
          return;
        }

        _doRequest('zipSearch?zip=' + zipCode
          , function(output) { 
              callback(_transformMarketList(output))
            }
          , errorHandler
         );    
    }

    , _fetchIdsByLocation = function(/* float */ latitude, /* float */ longitude, /* function */ callback, /* function */ errorHandler) {
        if(!latitude || !longitude) {
          return;
        }

        _doRequest('locSearch?lat=' + latitude + '&lng=' + longitude
          , function(output) {
              callback(_transformMarketList(output))
            }
          , errorHandler); 
      }

    , _fetchDetailsForMarketId = function(/* integer */ id, /* function */ callback, /* function */ errorHandler) {
        if(!id) {
          return;
        }

        _doRequest('mktDetail?id=' + id
          , function(output) {
              callback(_transformMarketDetails(output))
            }
          , errorHandler);
      }
      /**
       * Actually make the request.  Will use the cached result if available
       */
    , _doRequest = function(/* string */ actionPath, /* function */ callback, /* function */ errorHandler) {
        var options = _getOptions(actionPath)
        , cacheValue = _cache.get(actionPath);

        if(cacheValue) {
          console.log("Serving response from cache for query " + actionPath);
          return callback(cacheValue);
        }
        
        _request.get(options, function(error, response, json) {
          if(!error && response.statusCode == 200) {
            _cache.put(actionPath, json);
            callback(json);
          } else {
            console.log('Error in fetcher._doRequestError');
            console.log(error);
            if(errorHandler) {
              errorHandler(error);
            }
          }
        });
      }
    
    /**
     * Get a request option object with the correct query
     */
    , _getOptions = function(/* string */ actionPath) {
        return {
          url : 'http://search.ams.usda.gov/farmersmarkets/v1/data.svc/' + actionPath
        , json: true
        };
      }
    
    /**
     * Transform the list of market data into something usable.
     * Here we:
     *  Separate the miles and name into their own fields
     */
    , _transformMarketList = function(/* json */ marketList ) {
        var innerResults = marketList.results
          , transformedResults = []
          , index;

          for(index = 0; index < innerResults.length; index++) {
            var entry = innerResults[index]
            ,  newEntry = {}
            ,  milesAndName = entry.marketname
            ,  firstSpaceIndex = milesAndName.indexOf(' ')
            ,  miles = milesAndName.substring(0, firstSpaceIndex)
            ,  name = milesAndName.substring(milesAndName.indexOf(' ') + 1)
            ;

            newEntry.id = entry.id;
            newEntry.name = name;
            newEntry.milesFromCenter = miles;

            transformedResults.push(newEntry);
          }

          return { results : transformedResults };
      }
      
      /**
       * The 'details' object returned usually contains malformed/useless data.
       * Here, we:
       *  Strip extraneous <br> tags from the schedule
       *  Extract latitude and longitude from the (broken) Google Maps link
       *  Convert the semicolon delimited "products" string into an array
       */
    , _transformMarketDetails = function(/* json */ details) {
        var innerDetails = details.marketdetails;

        return { details :
                 { address : innerDetails.Address
                 , schedule : _stripBrTags(innerDetails.Schedule)
                 , googleMapsLink : innerDetails.GoogleLink
                 , products : _getProductsArray(innerDetails.Products)
                 , coordinates : _extractLatLngFromGoogleLink(innerDetails.GoogleLink)
                 }
              }

    }

      /**
       * Convert the semicolon delimited "products" string into an array
       */
    , _getProductsArray = function(/* string */ productsString) {
        if(!productsString) {
          return [];
        }

        return productsString.split(';');
      }

      /**
       * Strip <br> tags from a string
       */
    , _stripBrTags = function(/* string */ stringToStrip) {
        return stringToStrip.replace(/<br\s*[\/]?>/gi, "");
      }

      /**
       * The Google Maps link is broken, but it contains the latitude and longitude
       * of the market.  Extract and return them.
       */
    , _extractLatLngFromGoogleLink = function(/* string */ googleMapsLink) {
        var latStart = googleMapsLink.indexOf('=') + 1
          , lngEnd = googleMapsLink.indexOf('(')
          , latLngString = googleMapsLink.substring(latStart, lngEnd)
          , unescapedLatLngString = unescape(latLngString)
          , latLng = unescapedLatLngString.split(',')
          , lat = latLng[0].trim()
          , lng = latLng[1].trim();

          return { latitude : lat, longitude : lng };
      }
    ;

  return {
    
    /**
     * Make a request to get farmers markets near a specific zip code
     * @param zipCode - A US zip code
     * @param callback - A callback to execute after the data is fetched.  Should take a single object argument.
     * @param errorHandler - A handler to call if something unexpected occurs.  Should take a single object argument.
     */
    fetchIdsByZipCode : function(/* integer */ zipCode, /* function */ callback, /* function */ errorHandler) {
      _fetchIdsByZipCode(zipCode, callback, errorHandler);
    }

    /** 
     * Make a request to get farmers markets near a specific latitude and longitude
     * @param latitude - A valid latitude value
     * @param longitude - A valid longitude value
     * @param callback - A callback to execute after the data is fetched.  Should take a single object argument.
     * @param errorHandler - A handler to call if something unexpected occurs.  Should take a single object argument.
     */
  , fetchIdsByLocation : function(/* float */ latitude, /* float */ longitude, /* function */ callback, /* function */ errorHandler) {
      _fetchIdsByLocation(latitude, longitude, callback, errorHandler);
    }
  
    /** 
     * Make a request to get the details for a specific farmers marker based on its id
     * @param id - A valid farmer's marker id
     * @param callback - A callback to execute after the data is fetched.  Should take a single object argument.
     * @param errorHandler - A handler to call if something unexpected occurs.  Should take a single object argument.
     */
   , fetchDetailsForMarketId : function(/* float */ id, /* function */ callback, /* function */ errorHandler) {
       _fetchDetailsForMarketId(id, callback, errorHandler);
    }
  };

})();
