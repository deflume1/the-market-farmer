#!/bin/bash

# AWS EC2 instance init script for Centos 7
# After the instance is setup, don't forget to:
#    Allocate and associate an Elastic IP
#    Update the DNS settings for the domain to point to the new instance
#    Immediately change the password for user deflume1
#
# @author deflume1
# @since 2/2015

# Update the machine, and install relevant packages
yum -y update
yum -y install vim
yum -y install git
yum -y install epel-release 
yum -y install nodejs
yum -y install npm

# Add deflume1 user, and grant it root privleges
useradd deflume1
echo "deflume1:starwars" | chpasswd
echo 'deflume1 ALL=(ALL:ALL) ALL' >> /etc/sudoers

# Remove default centos user
userdel -r centos

# Enable ssh password authentication
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl reload sshd

# Setup vim
echo "colorscheme evening" >> /home/deflume1/.vimrc
echo "set nu" >> /home/deflume1/.vimrc
echo "set shiftwidth=4" >> /home/deflume1/.vimrc
echo "set tabstop=4" >> /home/deflume1/.vimrc
echo "set expandtab" >> /home/deflume1/.vimrc
echo "set autoindent" >> /home/deflume1/.vimrc
echo "set smartindent" >> /home/deflume1/.vimrc

# Setup git
git config --global user.name "Christopher DeFlumeri"
git config --global user.email deflume1@gmail.com
git config --global core.editor vim
